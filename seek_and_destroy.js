function destroyer(arr) {
    args = Array.prototype.slice.call(arguments).slice(1);
    return arr.filter(function(each) {
        for (var a = 0; a < args.length; a++) {
            if (each === args[a]) {
                return false;
           }
        }
        return true;
    });
}

destroyer([1, 2, 3, 1, 2, 3], 2, 3);
