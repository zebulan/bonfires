function mutation(arr) {
    left = arr[0].toLowerCase();
    right = arr[1].toLowerCase();
    for (var a = 0; a < right.length; a++) {
        is_char = false;
        for (var b = 0; b < left.length; b++) {
            if (left[b] === right[a]) {
                is_char = true;
                break;
            }
        }
        if (is_char === false) return false;
    }
    return true;
}

console.log(mutation(['hello', 'hey']));
console.log(mutation(['Alien', 'line']));
