var dna = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'};

function pair(str) {
    var result = [];
    for (var a = 0; a < str.length; a++) {
        var letter = str[a];
        result.push([letter, dna[letter]]);
    }
    return result;
}

console.log(pair("GCG"));
