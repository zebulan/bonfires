function spinalCase(str) {
    var result = [];
    var word = [];
    function push_word(word) {
        if (word.length > 0) {
            result.push(word.join(''));
        }
    }

    for (var a = 0; a < str.length; a++) {
        var letter = str[a];
        var ascii_code = letter.charCodeAt();
        if (is_upper(ascii_code)) {
            push_word(word);
            word = [letter.toLowerCase()];
        } else if (is_lower(ascii_code)) {
            word.push(letter);
        } else {
            push_word(word);
            word = [];
        }
    }
    push_word(word);
    return result.join('-');
}

function is_upper(char_code) {
    return char_code >= 65 && char_code <= 90;
}

function is_lower(char_code) {
    return char_code >= 97 && char_code <= 122;
}

console.log(spinalCase('This Is Spinal Tap'));
console.log(spinalCase('thisIsSpinalTap'));
