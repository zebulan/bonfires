function largestOfFour(arr) {
    result = [];
    for (var a = 0; a < arr.length; a++) {
        row = arr[a];
        row_largest = null;
        for (var b = 0; b < row.length; b++) {
            column = row[b];
            if (row_largest === null || column > row_largest) {
                row_largest = column;
            }
        }
        result.push(row_largest);
    }
    return result;
}

largestOfFour([[4, 5, 1, 3], [13, 27, 18, 26], [32, 35, 37, 39], [1000, 1001, 857, 1]]);
largestOfFour([[-4, -3, -2, -1], [-5, -4, -3, -2], [-6, -5, -4, -3], [-7, -6, -5, -4]]);
