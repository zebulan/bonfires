function palindrome(str) {
	var lower = str.toLowerCase().replace(/[^a-z]/g, '');
	return lower === lower.split('').reverse().join('');
}

console.log(palindrome("eye"));
console.log(palindrome("A man, a plan, a canal. Panama"));
