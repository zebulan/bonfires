function titleCase(str) {
    var words = str.toLowerCase().split(' ');
    for (var a = 0; a < words.length; a++) {
        var word = words[a];
        words[a] = word[0].toUpperCase() + word.slice(1);
    }
    return words.join(' ');
}

console.log(titleCase("I'm a little tea pot"));