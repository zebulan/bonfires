function drop(arr, func) {
  var first = arr[0];
  while (true) {
    if (func(first) === true || arr.length === 0) {
      return arr;
    }
    arr.shift();
    first = arr[0];
  }
}

console.log(drop([1, 2, 3, 4], function(n) {return n >= 3; }));  // [3, 4]
console.log(drop([1, 2, 3], function(n) {return n > 0; }));  // [1, 2, 3]
console.log(drop([1, 2, 3, 4], function(n) {return n > 5; }));  // []
