// function bouncer(arr) {
//     var result = [];
//     for (var a = 0; a < arr.length; a++) {
//         var item = arr[a];
//         if (Boolean(item) === true) {
//             result.push(item);
//         }
//     }
//     return result;
// }

function bouncer(arr) {
    return arr.filter(function (item) {
        return Boolean(item) === true;
    });
}

console.log(bouncer([7, 'ate', '', false, 9]));
console.log(bouncer([false, null, 0]));
