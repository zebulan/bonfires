roman_numerals = [[1000, 'M'], [900, 'CM'], [500, 'D'], [400, 'CD'],
                  [100, 'C'], [90, 'XC'], [50, 'L'], [40, 'XL'],
                  [10, 'X'], [9, 'IX'], [5, 'V'], [4, 'IV'], [1, 'I']];

function convert(num) {
    var result = [];
    for (var a = 0; a < roman_numerals.length; a++) {
        var value = roman_numerals[a][0];
        var multiplier = Math.floor(num / value);  // number of roman numerals
        num -= multiplier * value;  // remainder
        if (multiplier > 0) {
            var digit = roman_numerals[a][1];
            for (var b = 0; b < multiplier; b++) {
                result.push(digit);
            }
        }
    }
    return result.join('');
}

convert(36);
