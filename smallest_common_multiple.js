function smallestCommons(arr) {
  arr.sort();
  var range = [];
  var range_total = 0;
  for (a = arr[0]; a <= arr[1]; a++) {
    range.push(a);
    range_total++;
  }
  var result = 1;
  var is_common;
  while (true) {
    is_common = 0;
    for (b = 0; b < range_total; b++) {
      if (result % range[b] === 0) {
        is_common++;
      }
    }
    if (is_common === range_total) {
      return result;
    }
    result++;
  }
}

// console.log(smallestCommons([1, 5])); // 60
// console.log(smallestCommons([1, 13])); // 360360
