function chunk(arr, size) {
    var result = [];
    for (var a = 0; a < arr.length; a += size) {
        result.push(arr.slice(a, a + size));
    }
    return result;
}

console.log(chunk(['a', 'b', 'c', 'd'], 2));
