function fearNotLetter(str) {
    var last = str[0].charCodeAt();
    for (var a = 1; a < str.length; a++) {
        var current = str[a].charCodeAt();
        if (current !== last + 1) {
            return String.fromCharCode(last + 1);
        }
        last = current;
    }
}

console.log(fearNotLetter('abce'));
