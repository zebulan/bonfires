function unite(arr1, arr2, arr3) {
    var two_three = arr2.concat(arr3);
    console.log(two_three);
    for (var a = 0; a < two_three.length; a++) {
        current = two_three[a];
        is_unique = true;
        for (var b = 0; b < arr1.length; b++) {
            if (arr1[b] === current) {
                is_unique = false;
                break;
            }
        }
        if (is_unique) {
            arr1.push(current);
        }
    }
    return arr1;
}

console.log(unite([1, 2, 3], [5, 2, 1, 4], [2, 1]));  // 1, 2, 3, 5, 4
