function findLongestWord(str) {
	var longest = 0;
	var words = str.split(' ');
	console.log(words);
	for (a = 0; a < words.length; a++) {
		var word = words[a].length;
		if (word > longest) {
			longest = word;
		}
	}
	return longest;
}

console.log(findLongestWord('The quick brown fox jumped over the lazy dog'));
