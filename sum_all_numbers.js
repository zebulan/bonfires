function sumAll(arr) {
    arr.sort(function(a, b) {
        return a - b;
    });
    var total = 0;
    for (var a = arr[0]; a <= arr[1]; a++) {
        total += a;
    }
    return total;
}

console.log(sumAll([1, 4]));
console.log(sumAll([10, 5]));
