function diff(arr1, arr2) {
    return unique(arr1, arr2).concat(unique(arr2, arr1));
}

function unique(arr1, arr2) {
    return arr1.filter(function(obj) {
        return arr2.indexOf(obj) == -1;});
}

console.log(diff([1, 2, 3, 5], [1, 2, 3, 4, 5]));
