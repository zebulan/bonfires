function sumPrimes(num) {
  var sumOfPrimes = 0;
  var is_prime;
  for (a = 2; a <= num; a++) {
    is_prime = true;
    for (b = 2; b < Math.floor(Math.sqrt(a)) + 1; b++) {
      if (a % b === 0) {
        is_prime = false;
        break;
      }
    }
    if (is_prime === true) {
      sumOfPrimes += a;
    }
  }
  return sumOfPrimes;
}

// sumPrimes(10);  // 17
// sumPrimes(977);  // 73156
