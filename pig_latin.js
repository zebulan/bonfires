var vowels = 'aeiouy';

function translate(str) {
    var result = [];
    var words = str.split(' ');

    for (var a = 0; a < words.length; a++) { // word in words
        var sliceIndex = 0;
        var suffix = '';
        var word = words[a];
        for (var b = 0; b < word.length; b++) { // letter in word
            var letter = word[b];
            is_vowel = vowels.indexOf(letter);
            if (is_vowel !== -1) { // vowel
                if (b === 0) { // first letter is a vowel -> 'way'
                    suffix = 'w';
                } else {
                    sliceIndex = b; // to start slice below
                }
                suffix += 'ay';
                break;
            } else { // consonant
                suffix += letter;
            }
        }
        result.push(word.slice(sliceIndex) + suffix);
    }
    return result.join(' ');
}

translate('hello authority');
translate("consonant");
