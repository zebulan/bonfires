function boo(bool) {
    if (typeof bool == 'boolean') {
        return true;
    }
    return false;
}

console.log(boo(null));
console.log(boo(1));
