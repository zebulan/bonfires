function where(collection, source) {
    var result = [];
    var source_keys = Object.keys(source);
    for (var a = 0; a < collection.length; a++) {
        var all_keys_equal = true;
        var current = collection[a];
        for (var b = 0; b < source_keys.length; b++) {
            var source_key = source_keys[b];
            if (current.hasOwnProperty(source_key) === false ||
                current[source_key] !== source[source_key]) {
                all_keys_equal = false;
                break;
            }
        }
        if (all_keys_equal === true) {
            result.push(current);
        }
    }
    return result;
}

where([{ first: 'Romeo', last: 'Montague' }, { first: 'Mercutio', last: null },
    { first: 'Tybalt', last: 'Capulet' }], { last: 'Capulet' });
