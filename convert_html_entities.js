var html_entities = {
    "&": '&amp;',
    "<": '&gt',
    ">": '&gt',
    "'": '&apos',
    '"': '&quot'
};

function convert(str) {
    var result = [];
    for (var a = 0; a < str.length; a++) {
        current = str[a];
        if (html_entities.hasOwnProperty(current)) {
            result.push(html_entities[current]);
        } else {
            result.push(current);
        }
    }
    return result.join('');
}
console.log(convert('Dolce & Gabbana'));
