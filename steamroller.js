function steamroller(arr) {  // Thanks to 'kriskanya'
  return arr.reduce(function(a, b) {
    return a.concat(Array.isArray(b) ? steamroller(b) : b);
  }, []);
}

// function steamroller(arr) {
//   // I'm a steamroller, baby
//   var current;
//   var result = [];
//   for (a = 0; a < arr.length; a++) {
//     current = arr[a];
//     if (Array.isArray(current)) {
//       result = result.concat(steamroller(current));
//     } else {
//       result.push(current);
//     }
//   }
//   return result;
// }

console.log(steamroller([1, [2], [3, [[4]]]]));  // [1, 2, 3, 4]
