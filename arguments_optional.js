function add() {
  var arg = arguments[0];
  if (arguments.length === 1) {
    if (typeof(arg) === "number") {
      return function(z){
        if (typeof(z) === "number") {
          return arg + z;
        } else {
          return undefined;
        }
      };
    } else {
      return undefined;
    }

  } else {
    var arg2 = arguments[1];
    if (typeof(arg) === "number" && typeof(arg2) === "number") {
      return arg + arg2;
    } else {
      return undefined;
    }
  }
}

// console.log(add());
// console.log(add(2, '3'));
// console.log(add(2)([3]));
// var sum2And = add(2);
// console.log(sum2And(3));
