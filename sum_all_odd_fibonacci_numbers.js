function sumFibs(num) {
  var first = 0;
  var second = 1;
  var oddSum = 0;  // sum of odd fibonacci numbers
  var sum = 0;  // swap variable that sums two 'first' & 'second'
  while (second <= num) {
    if (second % 2 !== 0) {  // check if number is odd
      oddSum += second;
    }
    sum = first + second;
    first = second;
    second = sum;
  }
  return oddSum;
}

// sumFibs(4);  // 5
