function find(arr, func) {
  var current;
  for (a = 0; a < arr.length; a++) {
    current = arr[a];
    if (func(current) === true) {
      return current;
    }
  }
}

// console.log(find([1, 2, 3, 4], function(num){ return num % 2 === 0; })); // 2
